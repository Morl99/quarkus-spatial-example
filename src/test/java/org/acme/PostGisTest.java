package org.acme;

import io.quarkus.test.TestTransaction;
import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.persistence.GeoEntity;
import org.acme.persistence.GeoEntityRepository;
import org.acme.persistence.MyEntity;
import org.acme.persistence.MyEntityRepository;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Point;
import org.geolatte.geom.Polygon;
import org.geolatte.geom.builder.DSL;
import org.geolatte.geom.crs.CoordinateReferenceSystems;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;

import java.util.Set;

import static org.acme.geo.Geo.point;
import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

@QuarkusTest
@QuarkusTestResource(PostgresResource.class)
@TestTransaction
public class PostGisTest {

  @Inject GeoEntityRepository geoEntityRepository;

  @Test
  public void saveEntity() {
    GeoEntity entity = GeoEntity.builder().location(point(8.5, 50.5)).build();
    geoEntityRepository.persistAndFlush(entity);
    Long id = entity.getId();
    geoEntityRepository.getEntityManager().clear();
    assertThat(geoEntityRepository.findById(id), hasProperty("id", not(nullValue())));
  }

  @Test
  public void givenEntityWithinRange_whenFindWithin_expectEntity() {
    Polygon<G2D> square =
      DSL.polygon(WGS84, DSL.ring(g(9, 51), g(8, 51), g(8, 50), g(9, 50), g(9, 51)));
    Point<G2D> pointWithinSquare = point(8.5, 50.5);

    GeoEntity entity = GeoEntity.builder().location(pointWithinSquare).build();
    geoEntityRepository.persistAndFlush(entity);


    Set<GeoEntity> result = geoEntityRepository.findWithin(square);
    assertThat(result.size(), is(1));
  }

  @Test
  public void givenNoEntityWithinRange_whenFindWithin_expectEmptyResult() {
    Polygon<G2D> square =
      DSL.polygon(WGS84, DSL.ring(g(9, 51), g(8, 51), g(8, 50), g(9, 50), g(9, 51)));
    Point<G2D> pointOutsideSquare = point(9.5, 51.5);

    GeoEntity entity = GeoEntity.builder().location(pointOutsideSquare).build();
    geoEntityRepository.persistAndFlush(entity);


    Set<GeoEntity> result = geoEntityRepository.findWithin(square);
    assertThat(result.size(), is(0));
  }
}
