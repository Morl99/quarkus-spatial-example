package org.acme;

import io.quarkus.test.common.QuarkusTestResourceLifecycleManager;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.utility.DockerImageName;

import java.util.Collections;
import java.util.Map;

public class PostgresResource implements QuarkusTestResourceLifecycleManager {
  static DockerImageName postgisImage =
      DockerImageName.parse("postgis/postgis:13-3.1-alpine").asCompatibleSubstituteFor("postgres");

  static PostgreSQLContainer<?> db = new PostgreSQLContainer<>(postgisImage);

  @Override
  public Map<String, String> start() {
    db.start();
    return Map.of(
        "quarkus.datasource.jdbc.url",
        db.getJdbcUrl(),
        "quarkus.datasource.username",
        db.getUsername(),
        "quarkus.datasource.password",
        db.getPassword());
  }

  @Override
  public void stop() {
    db.stop();
  }
}
