package org.acme;

import io.quarkus.test.common.QuarkusTestResource;
import io.quarkus.test.junit.QuarkusTest;
import org.acme.persistence.MyEntity;
import org.acme.persistence.MyEntityRepository;
import org.junit.jupiter.api.Test;

import javax.inject.Inject;
import javax.transaction.Transactional;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.beans.HasPropertyWithValue.hasProperty;

@QuarkusTest
@QuarkusTestResource(PostgresResource.class)
public class PersistenceTest {

    @Inject
    MyEntityRepository myEntityRepository;

    @Test
    @Transactional
    public void saveEntity() {
        MyEntity entity = MyEntity.builder().name("test").build();
        myEntityRepository.persistAndFlush(entity);
        myEntityRepository.getEntityManager().clear();
        assertThat( myEntityRepository.findByName("test"), hasProperty("id", not(nullValue())));
    }


    @Test
    @Transactional
    public void findById() {
        //given
        MyEntity test = MyEntity.builder().name("test").build();
        myEntityRepository.persistAndFlush(test);
        myEntityRepository.getEntityManager().clear();
        MyEntity returnedEntity = myEntityRepository.findById(test.getId());
        assertThat(returnedEntity.getName(), is("test"));
    }

}
