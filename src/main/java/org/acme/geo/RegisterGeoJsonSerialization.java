package org.acme.geo;

import com.fasterxml.jackson.databind.ObjectMapper;
import io.quarkus.jackson.ObjectMapperCustomizer;
import org.geolatte.geom.json.GeolatteGeomModule;
import org.geolatte.geom.json.Setting;

import javax.inject.Singleton;

@Singleton
public class RegisterGeoJsonSerialization implements ObjectMapperCustomizer {

  public void customize(ObjectMapper mapper) {
    GeolatteGeomModule module = new GeolatteGeomModule();
    module.set(Setting.SUPPRESS_CRS_SERIALIZATION, true);
    mapper.registerModule(module);
  }
}
