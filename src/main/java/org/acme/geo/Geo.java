package org.acme.geo;

import org.geolatte.geom.G2D;
import org.geolatte.geom.Point;
import org.geolatte.geom.builder.DSL;

import static org.geolatte.geom.builder.DSL.g;
import static org.geolatte.geom.crs.CoordinateReferenceSystems.WGS84;

public class Geo {
  public static Point<G2D> point(double v, double v2) {
    return DSL.point(WGS84, g(v, v2));
  }
}
