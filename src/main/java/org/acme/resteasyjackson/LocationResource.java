package org.acme.resteasyjackson;

import org.acme.persistence.GeoEntity;
import org.acme.persistence.GeoEntityRepository;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.Set;
import java.util.stream.Collectors;

import static org.acme.geo.Geo.point;

@Path("/locations")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
public class LocationResource {

  final GeoEntityRepository geoEntityRepository;

  // Dirty hack to generate some test data in case the database is empty
  @Inject
  public LocationResource(GeoEntityRepository geoEntityRepository) {
    this.geoEntityRepository = geoEntityRepository;
    if (geoEntityRepository.count() == 0) {
      geoEntityRepository.generateTestData();
    }
  }

  @GET
  public Set<GeoEntity> list() {
    return geoEntityRepository.findAll().stream().collect(Collectors.toSet());
  }

  @GET
  @Path("{id}")
  public GeoEntity get(@PathParam("id") Long id) {
    GeoEntity geoEntity = geoEntityRepository.findById(id);
    if (geoEntity == null) {
      throw new WebApplicationException(404);
    }
    return geoEntity;
  }

  @POST
  public Response add(GeoEntity geoEntity) {
    geoEntityRepository.persist(geoEntity);
    return Response.created(URI.create("/locations/" + geoEntity.getId())).build();
  }

  @DELETE
  @Path("{id}")
  public Response delete(@PathParam("id") Long id) {
    geoEntityRepository.deleteById(id);
    return Response.noContent().build();
  }
}
