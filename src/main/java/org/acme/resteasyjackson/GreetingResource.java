package org.acme.resteasyjackson;

import org.acme.config.ConfigResource;

import javax.inject.Inject;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class GreetingResource {
  @Inject ConfigResource configResource;

  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String hello() {
    return String.format("Hello RESTEasy: %s", configResource.supersonic()) ;
  }
}
