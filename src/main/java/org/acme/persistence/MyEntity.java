package org.acme.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.SuperBuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class MyEntity {
    @Id @GeneratedValue
    Long id;

    String name;
}
