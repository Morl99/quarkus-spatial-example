package org.acme.persistence;

import io.quarkus.hibernate.orm.panache.PanacheRepository;
import io.quarkus.panache.common.Parameters;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;

import javax.enterprise.context.ApplicationScoped;
import javax.transaction.Transactional;
import java.util.Set;
import java.util.stream.Collectors;

import static io.quarkus.panache.common.Parameters.with;
import static org.acme.geo.Geo.point;

@ApplicationScoped
public class GeoEntityRepository implements PanacheRepository<GeoEntity> {
  @Transactional
  public void generateTestData( ) {
      final Set<GeoEntity> geoEntities = Set.of(
        GeoEntity.builder().location(point(8.54782, 50.547845)).build(),
        GeoEntity.builder().location(point(8.47785, 50.5874568)).build());
      persist(geoEntities);
  }
  public Set<GeoEntity> findWithin(Geometry<G2D> area) {
    return find("within(location, :area) = true", with("area", area)).stream().collect(Collectors.toSet());
  }
}
