package org.acme.persistence;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.experimental.SuperBuilder;
import org.geolatte.geom.G2D;
import org.geolatte.geom.Geometry;

import javax.annotation.Generated;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
@Data
@SuperBuilder
@NoArgsConstructor
public class GeoEntity {

  @GeneratedValue @Id Long id;
  @NonNull Geometry<G2D> location;
}
