package org.acme.persistence;

import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class MyEntityRepository implements PanacheRepository<MyEntity> {

    public MyEntity findByName(String name){
        return find("name", name).firstResult();
    }
}
